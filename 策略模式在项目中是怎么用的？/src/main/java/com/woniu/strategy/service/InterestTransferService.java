package com.woniu.strategy.service;


import com.woniu.strategy.CodeEnum;
import com.woniu.strategy.TransferService;
import org.springframework.stereotype.Service;

@Service
public class InterestTransferService implements TransferService {
    @Override
    public String transfer() {
        return "interest";
    }

    @Override
    public CodeEnum transCode() {
        return CodeEnum.INTEREST;
    }
}
