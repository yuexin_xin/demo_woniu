package com.woniu.dao.slave;

import com.woniu.vo.User;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface SlaveUserDao {

    int insertUser(User user);

}
