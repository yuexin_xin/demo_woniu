package com.example.demo.component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;

public class CacheMonitoring {
    public static void main(String[] args) throws Exception {
        RemovalListener<String, String> removalListener = notification -> {
            System.out.println("Removed: " + notification.getKey() + ", Cause: " + notification.getCause());
        };
        LoadingCache<String, String> cache = CacheBuilder
                .newBuilder()
                .maximumSize(100)
                .removalListener(removalListener)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return key;
                    }
                });
        String value  = cache.get("key");
        System.out.println(value);
        cache.cleanUp();
    }
}
