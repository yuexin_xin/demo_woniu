package com.woniu.service;


import com.woniu.anno.MainTransaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
public class MainService {

    @Resource
    private SonService sonService;

    @MainTransaction(3) //调用的方法中sonMethod1/sonMethod2/sonMethod3使用@Async开启了线程, 所以参数为: 3
    @Transactional(rollbackFor = Exception.class)
    public void test1() {
        sonService.sonMethod1("蜗牛", Thread.currentThread());
        sonService.sonMethod2("乾隆", "山治", Thread.currentThread());
        sonService.sonMethod3("小宝", Thread.currentThread());
        sonService.sonMethod4("罗宾");
    }


}