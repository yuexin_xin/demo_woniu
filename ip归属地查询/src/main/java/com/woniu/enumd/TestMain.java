package com.woniu.enumd;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//提升Java代码简洁度的小技巧
public class TestMain {
    /**
     * 先引入guava依赖
     * <dependency>
     * <groupId>com.google.guava</groupId>
     * <artifactId>guava</artifactId>
     * <version>29.0-jre</version>
     * </dependency>
     *
     * @param args
     */
    public static void main(String[] args) {
        //创建一个list集合并赋值
        List<String> stringList = new ArrayList<>();
        stringList.add("jack");
        stringList.add("pony");
        stringList.add("ekko");

        //简单改造：
        List<String> stringList2 = new ArrayList<String>(4) {{
            add("jack");
            add("pony");
            add("ekko");
        }};


        List<String> stringList3 = ImmutableList.of("jack", "pony", "ekko");
        // 类似于 Arrays.asList("jack", "pony", "ekko");
        stringList3.add("maomao");
        System.out.println(stringList3);

        // 终极改造：
        List<String> stringList4 = Lists.newArrayList("jack", "pony", "ekko");
        stringList4.add("hahah");
        System.out.println(stringList4);


        //去除list中的空值
        List<String> nameList = new ArrayList<>();
        List<String> noNullList = new ArrayList<>();
        nameList.add("jack");
        nameList.add("pony");
        nameList.add("ekko");
        nameList.add(null);
        for (String o : nameList) {
            if (o != null) {
                noNullList.add(o);
            }
        }


        //使用lamda简化后的写法
        List<String> noNullListFun = nameList
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());


        //list中的值求和
        List<BigDecimal> numList = ImmutableList.of(BigDecimal.valueOf(111L)
                , BigDecimal.valueOf(8888.22), BigDecimal.valueOf(333.22)
                , BigDecimal.valueOf(857857.22), BigDecimal.valueOf(5331.22));

        BigDecimal total = BigDecimal.ZERO;
        for (BigDecimal num : numList) {
            total = total.add(num);
        }
        System.out.println(total);


      // 简化一下：
        List<BigDecimal> numListSimple = ImmutableList.of(BigDecimal.valueOf(111L)
                , BigDecimal.valueOf(8888.22), BigDecimal.valueOf(333.22)
                , BigDecimal.valueOf(857857.22), BigDecimal.valueOf(5331.22));

        BigDecimal reduce = numListSimple.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(reduce);



        /*
         * 从多个集合中取匹配的值
         * 匹配到姓马的并去重然后放入一个新的集合中
         */
        List<String> nameListA = ImmutableList.of("素云云", "马云云", "腾花花", "阿娇娇", "马飞飞", "廖妹妹");
        List<String> nameListB = ImmutableList.of("素云涛", "唐三三", "小五五", "马中山", "马僻静", "马肥羊");
        List<String> nameListC = ImmutableList.of("张三", "李四", "王二", "麻子", "上官玩儿", "马可菠萝");
        Set<String> nameSet = new HashSet<>();
        nameListA.forEach(n -> {
            if (n.startsWith("马")) {
                nameSet.add(n);
            }
        });
        nameListB.forEach(n -> {
            if (n.startsWith("马")) {
                nameSet.add(n);
            }
        });
        nameListC.forEach(n -> {
            if (n.startsWith("马")) {
                nameSet.add(n);
            }
        });
        System.out.println(nameSet.toString());

       //改造后的代码：
        Set<String> set = Stream.of(nameListA, nameListB, nameListC)
                .flatMap(list -> list.stream().filter(name -> name.startsWith("马")))
                .collect(Collectors.toSet());

        System.out.println(set);


    }

    private static void eatWhat(String animalType) {
//        if ("Dog".equals(animalType)) {
//            System.out.println("吃骨头");
//        } else if ("Cat".equals(animalType)) {
//            System.out.println("我想吃鱼了");
//        } else if ("Sheep".equals(animalType)) {
//            System.out.println("吃草");
//        } else {
//            System.out.println("不知道吃啥");
//        }


    }


}
