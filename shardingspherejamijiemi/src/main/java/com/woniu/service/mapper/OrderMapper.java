package com.woniu.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.service.entity.Order;

/**
 * <p>
 *  状态枚举类
 * </p>
 *
 * @author 公众号：【程序员蜗牛g】
 */
public interface OrderMapper extends BaseMapper<Order> {

}
