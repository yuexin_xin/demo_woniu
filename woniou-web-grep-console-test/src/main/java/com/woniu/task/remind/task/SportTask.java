package com.woniu.task.remind.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author 蜗牛
 * @description 每天进步一点点
 * @date 2023/03/18 13:48
 */
@Slf4j
@Component
public class SportTask  {

    @Scheduled(cron = "0/5 * * * * ?")
    public void execute() {

        log.info("[运动提醒]主人,站起来活动一下吧"+ LocalDateTime.now());
    }
}
