package com.woniu.mp.mapstruct.four;

import org.springframework.stereotype.Component;

@Component
public class SimpleService {
    public String formatName(String name) {
        return "您的名字是：" + name;
    }
}
