package com.woniu.logger.conf;


/**
 * 功能描述: 日志级别枚举 <br/>
 */
public enum LoggerLevelEnum {

    /**
     * 不打印
     */
    NONE,
    /**
     * 打印正常
     */
    PRINT,
    /**
     * 打印异常
     */
    ERROR,
    /**
     * 全部打印
     */
    ALL,

}
