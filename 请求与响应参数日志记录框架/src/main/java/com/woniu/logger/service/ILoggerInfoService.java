package com.woniu.logger.service;

/**
 * @className: ILoggerInfoService
 * @author: woniu
 * @date: 2023/8/24
 **/

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.logger.entity.LoggerInfoEntity;
import org.springframework.scheduling.annotation.Async;

/**
 * 功能描述: 日志服务接口 <br/>
 */
public interface ILoggerInfoService extends IService<LoggerInfoEntity> {

    /**
     * 功能描述: 异步保存 <br/>
     *
     * @param info 日志信息
     */
    @Async
    default void saveAsync(LoggerInfoEntity info) {
        save(info);
    }

}

