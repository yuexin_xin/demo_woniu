package com.woniu.commonswitch.service;

import com.woniu.commonswitch.annotation.ServiceSwitch;
import com.woniu.commonswitch.constant.Constant;
import com.woniu.commonswitch.util.Result;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class RegService {

	/**
	 * 下单
	 */
	@ServiceSwitch(switchKey = Constant.ConfigCode.REG_PAY_SWITCH)
	public Result createOrder() {

		// 具体下单业务逻辑省略....

		return new Result(HttpStatus.OK.value(), "挂号下单成功");
	}
}
