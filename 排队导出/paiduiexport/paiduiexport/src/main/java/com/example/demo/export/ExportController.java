package com.example.demo.export;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ThreadPoolExecutor;

@RestController
@RequestMapping("/export")
@Slf4j
public class ExportController {

    @Autowired
    private ExportImpl export;

    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 实现一个排队导出功能
     */
    @PostMapping("/exportFile")
    public void exportFile() {
        log.info("提交线程池任务");
        threadPoolExecutor.execute(() -> {
                    ExportUser sysUser = new ExportUser();
                    sysUser.setUserName(Thread.currentThread().getName());
                    try {
                        export.export(sysUser);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
        log.info("提交线程池任务结束");
    }
}
